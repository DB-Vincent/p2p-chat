# P2P-Chat

Een peer-to-peer chat in Python. Een project voor python lessen in de Systems, Services & Security op de Thomas More hogeschool.

## Getting Started

Om snel te starten kan je volgende commando's uitvoeren.  
`git clone https://gitlab.com/DB-Vincent/p2p-chat.git`  
`cd p2p-chat`  
`pip install -r requirements.txt`  

### Prerequisites

[Python 3.x](https://www.python.org/downloads/)

### Installing

De code up and running krijgen doe je als volgt:
`python main.py`

## Authors

* **Kevin van Merode** - *Code Writing* 
* **Vincent De Borger** - *Code Writing*

## License

This project is licensed under the Apache 2.0 License - see the [LICENSE.md](LICENSE.md) file for details

