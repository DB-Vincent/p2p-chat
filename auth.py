import pymongo
import sys
import bcrypt

def login(db): # Login function
    users = db.users # Select the collection
    
    username = str(input("Enter your username: ")) # Read username (as string)
    password = str(input("Enter your password: ")) # Read password (as string)

    try:
        user = users.find_one({ "username": username.lower() }) # Search the database for a user with that username
    except:
        print("Something went wrong while getting a user with that username.") # If we can't find the user in the database, give an error message.

    count = 0 # counter for the tries
    count = int(count)
    while(not(bcrypt.checkpw(password.encode('utf8'), user['password']))): # Check if the user's password equals the given password
        if (count < 3): # Check if the user has tried more than 3 times already
            tries = 3-count # Amount of tries
            print("Sorry, the username & password combination you've given is incorrect! Please try again. You have %i tries left" % tries)
            password = str(input("Enter your password: ")) # Ask for the password again
            count += 1
        else:
            print("You've entered you're password wrong too many times!") # Stop the program and say that the wrong password has been given too many times
            sys.exit()

    return user['displayname']

def register(db): # Register function
    users = db.users
    
    username = str(input("Enter the username you want to use: ")) # Ask the user to enter their username
    password = str(input("Enter your password: ")) # Ask the user to enter their password
    password_again = str(input("Enter your password again: ")) # Ask it for the second time
    while (password_again != password): # While the passwords don't match
        print("Sorry, the password you entered does not equal the password you entered before. Try again.")
        password = str(input("Enter your password: ")) # Ask the user to enter their password
        password_again = str(input("Enter your password again: ")) # Ask to enter the password again

    
    while (users.find_one({ "username": username.lower() })): # Look if the user already exists
        print("A user with that username already exists! Try again")
        
        username = str(input("Enter the username you want to use: ")) # Ask for username
        password = str(input("Enter your password: ")) # Ask the user to enter their password
        password_again = str(input("Enter your password again: ")) # Ask it for the second time
        while (password_again != password): # While the passwords don't match
            print("Sorry, the password you entered does not equal the password you entered before. Try again.")
            password = str(input("Enter your password: ")) # Ask the user to enter their password
            password_again = str(input("Enter your password again: ")) # Ask to enter the password again

    try: # Try to add the user to the database with hashed password
        user = users.insert_one({ "displayname": username, "username": username.lower(), "password": bcrypt.hashpw(password.encode('utf8'), bcrypt.gensalt()) })
    except:
        print("Something went wrong while trying to add your account to our database.") # If insert_one errors, catch it here.
