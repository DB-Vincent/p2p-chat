import auth
import socket
import threading
import db
from p2p.server import Sender as server
from p2p.client import Receiver as client

def splash():
    logo = """
####################################################
#     ______  __  _    ______  __             __   #
#    / ____/ / / (_)  / ____/ / /_   ____ _  / /_  #
#   / /     / / / /  / /     / __ \ / __ `/ / __/  #
#  / /___  / / / /  / /___  / / / // /_/ / / /_    #
#  \____/ /_/ /_/   \____/ /_/ /_/ \__,_/  \__/    #
#                                                  #
####################################################                                                
"""
    print(logo) # Print the logo

def display_startup():
    print("Welcome to the CliChat client! You're almost ready to get chatting with your friends! I only need you to login.")
    acc_q = str(input("Do you already have an account? [Y/N]: ")) # Ask if the user already has an account and put that result in 'acc_q' (account question)

    while (acc_q.lower() != str("y") and acc_q.lower() != str("n")): # While none of the two possible answers are given, show error and ask again.
        print("Err, I didn't understand what you said, can you try again?")
        acc_q = str(input("Do you already have an account? [Y/N]: ")) # Ask if the user already has an account and put that result in 'acc_q' (account question)

    database = db.init() # Do connection things in db.py
    

    if (acc_q.lower() == str("y")): 
        print("Hell yeah! Let's log in.") # The user entered "y" stating he has an account
        user = auth.login(database) # Show login screen
        print("Welcome,", user)
        receiver = client(socket.gethostbyname(socket.gethostname()), 2033)
        peer = input("Where are we connecting to? ")
        sender = server(peer, 2033)
        threads = [receiver.start(), sender.start()]

    elif (acc_q.lower() == str("n")):
        print("So, it's your first time huh. Let's get yourself an account!") # The user entered "n" stating he want to create an account.
        auth.register(database) # Show register screen
        user = auth.login(database)
        print("Welcome,", user)
        receiver = client(socket.gethostbyname(socket.gethostname()), 2033)
        peer = input("Where are we connecting to? ")
        sender = server(peer, 2033)
        threads = [receiver.start(), sender.start()]
