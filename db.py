import pymongo
import json

def init():
    with open('config.json') as json_file: # Load the config.json file
        data = json.load(json_file) # Parse the config file

    db_name = data['mongo_db'] # Get the name of the database out of the config file
    client = pymongo.MongoClient(data['mongo_host'], data['mongo_port']) # Connect to the mongodb server based on info in the config file
    db = client[db_name] # Select the database
    return db
